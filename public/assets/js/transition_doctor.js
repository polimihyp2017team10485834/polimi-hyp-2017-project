/*
    Al caricamento della pagina (click su Doctors nel menu in alto del sito):

    - vengono mostrati i dottori in ordine alfabetico tramite la funzione filterBy("doctor")
    - viene creato il filtro relativo ai services tramite la funzione createSelect(serviceList, "service", "service");
    - viene creato il filtro relativo alle locations tramite la funzione createSelect(locationList, "location", "location");
*/

$(window).ready(function () {
     
    var array_name = new Array(0);
    var array_id = new Array(0);
    locationList = $("#locationList");
    serviceList = $("#serviceList");
    inputSurname = document.getElementById("inputSurname");

    console.log("Doctor id: "+URL().id);

    filterBy("doctor");

    createSelect(serviceList, "service", "service");
    createSelect(locationList, "location", "location");

});

/*
    filterByLocation e filterByService svolgono due operazioni:

    - puliscono il campo di testo per la ricerca dei doctor in ordne alfabetico
    - chiamano la funzione che filtra i dottori per il parametro richiesto dall'utente
      (la funzione richiede di specificare come secondo parametro nel formato #id il select che si vuole "ripristinare")
    
    filterByName:
    - ripristina i valori delle select impostando quello di default tramite la funzione val()
    - chiama la funzione per filtrare i dottori secondo la stringa inserita nell'apposito campo

*/

function filterByLocation () {
    $("#inputSurname").val("");
    filterBy("DocLoc/"+locationList.val(), "#serviceList");
}

function filterByService () {
    $("#inputSurname").val("");
    filterBy("DocInServ/"+serviceList.val(), "#locationList")
}

function filterByName () {
    $("#serviceList").val($("#serviceList option:first").val());
    $("#locationList").val($("#locationList option:first").val());
    filterBy("doctor/?surname="+$("#inputSurname").val().charAt(0).toUpperCase() + $("#inputSurname").val().slice(1));
}


function filterBy (url, id) {
    
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/"+url,
        url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/"+url,
        
        success: function (response) {
            //rimuovo le righe relative alla precedente interrogazione del database
            $("#docRowDoctor").find("tr").remove();
            
            //pulisco i campi dei filtri che non utilizzo
            $(id).val($(id+" option:first").val());
            
            for (var i in response) {
                console.log(response[i].surname);
                //aggiungo una riga alla "tabella" contenente i dottori da mostrare
                $("#docRowDoctor").append(
                        "<tr class='row-transition'>"+
                            "<td class = 'docRow' > <a id = 'link" + (Number(i)+1) + "' href='../pages/doctors.html?id=" +response[i].id +
                            "' class='text-center-trans'>"+ response[i].surname+" " + response[i].name + "</a> </td>"+
                            "<td> <img src='"+response[i].image+"' class='img pos-img' /> </td>"+
                        "</tr>"
                    );
            }
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });
    
}

function createSelect (list, url, description) {
    //leggo le location presenti per creare il select
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/"+url,
        url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/"+url,

        success: function (response) {

            //memorizzo in due array le informazioni che mi servono per costruire la SELECT
            array_name = new Array (0);
            array_id = new Array (0);


            for(var i in response) {
                array_name.push(response[i].name);
                array_id.push(response[i].id);
            }
            
            //inserisco nella SELECT l'opzione di default
            var firstOption = document.createElement("option");
            firstOption.value = "";
            firstOption.text = " -- filter by "+description+" -- ";
            list.append(firstOption);

            //creo e appendo nella SELECT le opzioni lette da DB 
            for (var i = 0; i < array_name.length; i++) {
                var option = document.createElement("option");
                option.value = array_id[i];
                option.text = array_name[i];
                list.append(option);
            }


        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });
}

function resetFilter () {
    $("#inputSurname").val("");
    filterByName();
}