$(window).ready(function () {
    
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/whoweare",
		url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/whoweare",
        
        success: function (response) {
            
            for (var i in response) {
                console.log(response[i].title);
                
                //creo il menu laterale con le ancore interne
                $("#menuWWA").append(
                        "<a href='#"+response[i].id+"' class='menuwho text-red'>"+response[i].title+"</a>"
                );
                
                //creo il contenuto della pagina
                $("#whoweare").append(
                        "<div> <a name='"+response[i].id+"' class='anchor'></a><h2>"+response[i].title+"</h2>"+
                        "<img src='"+response[i].image+"' class= 'img topic-img-service' /> <p class='topic-p top-distance'>"+response[i].description+"</p>"+
                        "</div><hr />"
                    );	
    
            }
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });

});