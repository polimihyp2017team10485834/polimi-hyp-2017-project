/*
    Funzione chiamata visualizzando i dottori operanti in un servizio accedendo dalla pagina di un servizio.
    
*/

$(window).ready(function () {
       
    console.log("Doctor id: "+URL().id);

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/DocInServ/"+URL().id,
		url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/DocInServ/"+URL().id,
        
        success: function (response) {
            for(var i in response) {
                
                $("#docRow").append(
                    "<tr class='row-transition'>"+
                        "<td class = 'docRow' > <a id = 'link" + (Number(i)+1) + "' href='../pages/doctors.html?id=" +response[i].id +
                        "' class='text-center-trans'>"+ response[i].surname+" " + response[i].name + "</a> </td>"+
                        "<td> <img src='"+response[i].image+"' class='img pos-img' /> </td>"+
                    "</tr>"
                );
                
            }
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });




});
