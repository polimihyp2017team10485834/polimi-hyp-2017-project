/*
    Funzione chiamata visualizzando i servizi offerti in una location accedendo dalla pagina di una location.
    
*/

$(window).ready(function () {
    
    console.log("Doctor id: "+URL().id);

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/ServLoc/"+URL().id,
		url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/ServLoc/"+URL().id,
        
        success: function (response) {
            for(var i in response) {
                $("#servRow").append(
                    "<tr class='row-transition'>"+
                        "<td class = 'docRow' > <a id = 'link" + (Number(i)+1) + "' href='../pages/services.html?id=" +response[i].id +
                        "' class='text-center-trans'>"+ response[i].name + "</a> </td>"+
                        "<td> <img src='"+response[i].image+"' class='img pos-img' /> </td>"+
                    "</tr>"
                );
            }
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });




});