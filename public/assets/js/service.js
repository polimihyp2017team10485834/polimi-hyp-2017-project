var service_name;
var service_description;
var service_image;
var service_location;
var doctor_resp;
var service_doctor;

$(window).ready(function () {
    
    console.log("Service id: "+URL().id);


	service_name = $("#serviceName");
    service_description = $("#service_description");
    service_image = $("#service_image");
	doctor_resp = $('#RespDoc');
	service_doctor= $('#DocInServ');

	service_doctor.attr("href", "../pages/transition_doctor2.html?id="+URL().id);
    
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/service/?id="+URL().id,
		url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/service/?id="+URL().id,
        success: function (response) {
            console.log(response[0].name);
            service_name.text(response[0].name);
			service_description.html(response[0].description);
			service_image.attr("src",response[0].image);
            
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });
	
	//ottengo le location dal servizio
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/locationByService/"+URL().id,
		url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/locationByService/"+URL().id,
        success: function (response) {
			for(var i in response) {
				$("#loc"+(Number(i)+1)).text(response[i].name);
				$("#loc"+(Number(i)+1)).attr("href","../pages/locations.html?id="+response[i].idL);
			}
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });
	
	$.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/RespDoc/"+URL().id,
        url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/RespDoc/"+URL().id,
        success: function (response) {		
				doctor_resp.attr("href","../pages/doctors.html?id="+response[0].idD);
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });

});

