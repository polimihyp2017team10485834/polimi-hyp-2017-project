var doctor_surname;
var doctor_image;
var doctor_description;
var doctor_service;
var doctor_responsible;

$(window).ready(function () {
    
    console.log("Doctor id: "+URL().id);


    doctor_surname = $("#name");
    doctor_image = $("#photo");
    doctor_description = $("#description");
    doctor_service = $("#service");
    doctor_responsible = $("#responsible");
 /*   doctor_services = $("#doctor_services");
    doctor_image = $("#doctor_image");
  */
    
    //ottengo i dati relativi al dottore
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/doctor/?id="+URL().id,
        url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/doctor/?id="+URL().id,
        success: function (response) {
            console.log(response[0].surname);
            doctor_surname.text(response[0].name+" "+response[0].surname);
            doctor_image.attr("src", response[0].image);
            doctor_description.html(response[0].description);
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });

    //ottengo il servizio del dottore
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/serviceByDoctor/"+URL().id,
        url:  "https://polimi-hyp-2017-team-10485834.herokuapp.com/serviceByDoctor/"+URL().id,
        success: function (response) {
            
            doctor_service.text(response[0].name);
            doctor_service.attr("href","../pages/services.html?id="+response[0].idS);
            if(URL().id == response[0].docRespID)
                doctor_responsible.attr("style", "display: visible;");
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });


});