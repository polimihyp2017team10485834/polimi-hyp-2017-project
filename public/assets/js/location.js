var location_name;
var location_description;
var location_image;
var location_serv;

$(window).ready(function () {
    
    console.log("Location id: "+URL().id);


	location_name = $("#locationName");
    location_description = $("#location_description");
    location_image = $("#location_image");
	location_serv = $("#ServLoc");
	location_serv.attr("href", "../pages/transition_service.html?id="+URL().id);
	
    
    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        //url: "http://localhost:8080/location/?id="+URL().id,
		url: "https://polimi-hyp-2017-team-10485834.herokuapp.com/location/?id="+URL().id,
        success: function (response) {
            console.log(response[0].image);
            location_name.text(response[0].name);
			location_description.html(response[0].description);
			location_image.attr("src",response[0].image);
            
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });
	
});
