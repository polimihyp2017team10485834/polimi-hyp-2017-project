// index.js

// call the packages we need
const express    = require('express');        // call express
const app        = express();                 // define our app using express
const bodyParser = require('body-parser');
const sqlDbFactory = require ('knex');
const process = require ('process');

let sqlDb;

function initSqlDB() {
  /* Locally we should launch the app with TEST=true to use SQLlite:

       > set TEST=true node ./index.js

    */
  if (process.env.TEST) {
    sqlDb = sqlDbFactory({
      client: "sqlite3",
      debug: true,
      connection: {
        filename: "./other/clinicdb.sqlite"     
      }
    });
  } else {
    sqlDb = sqlDbFactory({
      debug: true,
      client: "pg",
      connection: process.env.DATABASE_URL,
      ssl: true
    });
  }
}

//creazione tabella Doctor
function initDoctorTable() {
  return sqlDb.schema.hasTable('doctor').then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable('doctor', table => {
          table.increments();
          table.string('name');
		  table.string('surname');
		  table.string('image');
		  table.text('description');
		  })
        .then(() => {
          return Promise.all(
            _.map(doctorList, p => {
              delete p.id;
              return sqlDb('doctor').insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}

//creazione tabella Service
function initServiceTable() {
  return sqlDb.schema.hasTable('service').then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable('service', table => {
          table.increments();
          table.string('name');
          table.integer('docRespID');
		  table.string('image');
		  table.text('description');
		  })
        .then(() => {
          return Promise.all(
            _.map(serviceList, p => {
              delete p.id;
              return sqlDb('service').insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}

//creazione tabella Location
function initLocationTable() {
  return sqlDb.schema.hasTable('location').then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable('location', table => {
          table.increments();
          table.string('name');
		  table.string('image');
		  table.text('description');
		  })
        .then(() => {
          return Promise.all(
            _.map(locationList, p => {
              delete p.id;
              return sqlDb('location').insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}

//creazione tabella Location-Doctor
function initLDTable() {
  return sqlDb.schema.hasTable('location_doctor').then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable('location_doctor', table => {
          table.integer('idD');
		  table.integer('idL');
		  })
        .then(() => {
          return Promise.all(
            _.map(LDList, p => {
              delete p.id;
              return sqlDb('location_doctor').insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}

//creazione tabella Service-Doctor
function initSDTable() {
  return sqlDb.schema.hasTable('service_doctor').then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable('service_doctor', table => {
          table.integer('idD');
		  table.integer('idS');
		  })
        .then(() => {
          return Promise.all(
            _.map(SDList, p => {
              delete p.id;
              return sqlDb('service_doctor').insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}

//creazione tabella Service-Location
function initSLTable() {
  return sqlDb.schema.hasTable('service_location').then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable('service_location', table => {
          table.integer('idL');
		  table.integer('idS');
		  })
        .then(() => {
          return Promise.all(
            _.map(SLList, p => {
              delete p.id;
              return sqlDb('service_location').insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}

//creazione tabella WhoWeAre
function initWWATable() {
  return sqlDb.schema.hasTable('whoweare').then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable('whoweare', table => {
          table.increments();
          table.string('title');
		  table.string('image');
		  table.text('description');
		  })
        .then(() => {
          return Promise.all(
            _.map(WWAList, p => {
              delete p.id;
              return sqlDb('whoweare').insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}


let port = process.env.PORT || 8080;        // set our port

app.set('port', port);

const _ = require("lodash");
let doctorList = require("./other/doctor.json");
let serviceList = require("./other/service.json");
let locationList = require("./other/location.json");
let LDList = require("./other/location_doctor.json");
let SDList = require("./other/service_doctor.json");
let SLList = require("./other/service_location.json");
let WWAList = require("./other/whoweare.json");


app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//query relative alla tabella DOCTOR
app.get('/doctor', function(req, res) {
    let id = parseInt(_.get(req, "query.id", 0));
    let surname = _.get(req, "query.surname", "None");
    let myQuery=sqlDb('doctor');
    
    myQuery.orderBy('surname', 'asc');
    
    if(id !== 0)
        myQuery.where('id', id);
    if (surname !== "None")
        myQuery.where('surname', 'like', '%'+surname+'%');

    myQuery.then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//query relative alla tabella LOCATION
app.get('/location', function(req, res) {
    let id = parseInt(_.get(req, "query.id", 0));
    let myQuery=sqlDb('location');
    
    myQuery.orderBy('name', 'asc');
    
    if(id !== 0)
        myQuery.where('id', id);

    myQuery.then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//query relative alla tabella SERVICE
app.get('/service', function(req, res) {
    let id = parseInt(_.get(req, "query.id", 0));
    let myQuery=sqlDb('service');
    
    myQuery.orderBy('name', 'asc');
    
    if(id !== 0)
        myQuery.where('id', id);

    myQuery.then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//Query per WhoWeAre
app.get('/whoweare', function(req, res) {
    sqlDb('whoweare').then (result =>{
        res.send(JSON.stringify(result));
    });
    console.log(res);
});

//Query per il SERVICE dato il DOCTOR
app.get('/serviceByDoctor/:idD', function(req, res) {
    let id = parseInt(req.params.idD);
    sqlDb('service_doctor').innerJoin('service', 'service_doctor.idS', 'service.id').where('idD', id).then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//Query per le LOCATION dai SERVICE
app.get('/locationByService/:idS', function(req, res) {
    let id = parseInt(req.params.idS);
    sqlDb('service_location').innerJoin('location', 'service_location.idL', 'location.id').where('idS', id).then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//Query per i RESPDOC da SERVICE
app.get('/RespDoc/:idD', function(req, res) {
    let id = parseInt(req.params.idD);
    sqlDb('service_doctor').innerJoin('doctor', 'service_doctor.idD', 'doctor.id').where('idS', id).then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//Query per i SERVICE da LOCATION
app.get('/ServLoc/:idL', function(req, res) {
    let id = parseInt(req.params.idL);
    sqlDb('service_location').innerJoin('service', 'service_location.idS', 'service.id').where('idL', id).then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//Query per i DOCTOR in the SERVICE
//in ordine alfabetico perchè viene utilizzato nella pagina dei dottori elencati per ordine alfabetico
app.get('/DocInServ/:idS', function(req, res) {
    let id = parseInt(req.params.idS);
    sqlDb('service_doctor').innerJoin('doctor', 'service_doctor.idD', 'doctor.id').
    where('idS', id).orderBy('doctor.surname', 'asc').then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});

//Query per i DOCTOR in the LOCATION 
//in ordine alfabetico perchè viene utilizzato nella pagina dei dottori elencati per ordine alfabetico
app.get('/DocLoc/:idL', function(req, res) {
    let id = parseInt(req.params.idL);
    sqlDb('location_doctor').innerJoin('doctor', 'location_doctor.idD', 'doctor.id').
    where('idL', id).orderBy('doctor.surname', 'asc').then (result =>{
        res.send(JSON.stringify(result));   
    });
    console.log(res);
});


initSqlDB();
initDoctorTable();
initServiceTable();
initLocationTable();
initLDTable();
initSDTable();
initSLTable();
initWWATable();


app.listen(port);
console.log('Connected on port ' + port);
